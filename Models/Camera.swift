//
//  Camera.swift
//  The Jove Express
//
//  Created by David Giovannini on 7/15/21.
//

import Foundation
import BTbyJove

public struct FogIPAddress: FogExternalizable, Equatable {
	let a: UInt8
	let b: UInt8
	let c: UInt8
	let d: UInt8
	let p: UInt16
	let r: String
	
	public var fogSize: Int {
		6 + r.fogSize
	}
	
	public init() {
		a = 0
		b = 0
		c = 0
		d = 0
		p = 0
		r = ""
	}
	
	public init(fog data: Data, at cursor: inout Int) throws {
		a = try UInt8(fog: data, at: &cursor)
		b = try UInt8(fog: data, at: &cursor)
		c = try UInt8(fog: data, at: &cursor)
		d = try UInt8(fog: data, at: &cursor)
		p = try UInt16(fog: data, at: &cursor)
		r = try String(fog: data, at: &cursor)
	}
	
	public func write(fog data: inout Data) {
		a.write(fog: &data)
		b.write(fog: &data)
		c.write(fog: &data)
		d.write(fog: &data)
		p.write(fog: &data)
		r.write(fog: &data)
	}
	
	public var url: URL? {
		guard a != 0 || b != 0 || c != 0 || d != 0 else {
			return nil
		}
		return URL(string:"http://\(a).\(b).\(c).\(d):\(p)\(r.isEmpty ? "" : "/")\(r)")
	}
}

public enum CameraFPS: UInt8, FogExternalizable, CaseIterable, Hashable {
	case fps6 = 6
	case fps12 = 12
	case fps24 = 24
	case fps48 = 48
}

public struct Camera {
	public var fps: BTSubject<CameraFPS>
	public var power: BTSubject<Bool>
	public var url: BTSubject<FogIPAddress>
	
	public init(broadcaster: BTBroadcaster) {
		self.fps = BTSubject(
			BTCharacteristicIdentity(TrainCategory.camera, TrainPower.calibration),
			broadcaster,
			.fps6)
		self.power = BTSubject(
			BTCharacteristicIdentity(TrainCategory.camera, TrainPower.power),
			broadcaster,
			false)
		self.url = BTSubject(
			BTCharacteristicIdentity(TrainCategory.camera, TrainPower.state),
			broadcaster,
			FogIPAddress())
	}
	
	func reset() {
		fps.reset()
		power.reset()
		url.reset()
	}
}
