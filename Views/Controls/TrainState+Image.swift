//
//  TrainState+Image.swift
//  The Jove Express
//
//  Created by David Giovannini on 7/16/21.
//

import SwiftUI

extension TrainState {
	var image: Image {
		switch self {
		case .disconneted:
			return Image("Disconnected")
		case .connecting:
			return Image("Connecting")
		case .connected:
			return Image("Connected")
		case .dead:
			return Image("Dead")
		}
	}
}
