
from fog.FogMessage import FogMessage

from preferences import Preferences
	
class BTProperty:
	def __init__(self, uuid, value, serializer, deserializer, saves):
		self.uuid = uuid
		self.serializer = serializer
		self.deserializer = deserializer
		self.saves = saves
		self.notify = None

		def getValueBytes():
			nonlocal value
			nonlocal serializer
			if value is not None and serializer is not None:
				message = FogMessage.writing()
				serializer(message, value)
				return message.written()
			return []
		
		if saves:
			if deserializer is not None:
				self.encoded = Preferences.get(self.uuid, lambda: getValueBytes())
				message = FogMessage.reading(self.encoded if self.encoded else [])
				self.current = deserializer(message)
			else:
				self.current = Preferences.get(self.uuid, value)
				self.encoded = getValueBytes()
		else:
			self.current = value
			self.encoded = getValueBytes()
			
	ids = 0
	@classmethod
	def _newId(cls):
		cls.ids +=1
		return cls.ids
		
	def activate(self, peripheral, serviceIdx, receiving):
		self.receiving = receiving
		if receiving is not None:
			flags = ['read', 'notify', 'write-without-response']
			write_callback = self._receive
		else:
			flags = ['read', 'notify']
			write_callback = None
			
		peripheral.add_characteristic(
			srv_id = serviceIdx,
			chr_id = BTProperty._newId(),
			uuid = self.uuid,
			value = self.encoded,
			notifying = False,
			flags = flags,
			write_callback=self._receive,
			read_callback=self._read,
			notify_callback=self._notifyRequest)
			
	def __str__ (self):
		return self.uuid
		
	@property
	def value(self):
		return self.current
		
	@value.setter
	def value(self, newValue):
		self.current = newValue
		if newValue is not None:
			if self.serializer is not None:
				message = FogMessage.writing()
				self.serializer(message, newValue)
				self.encoded = message.written()
				toSave = self.encoded
			else:
				self.encoded = []
				toSave = newValue
		else:
			self.encoded = []
			toSave = None

		if self.saves:
			Preferences.set(self.uuid, toSave)
			
		if self.notify is not None:
			self.notify.set_value(self.encoded)
	
	def _notifyRequest(self, notifying, characteristic):
		if notifying is not None:
			self.notify = characteristic
		else:
			self.notify = None
		
	def _read(self):
		return self.encoded

	def _receive(self, value, options):
		if self.deserializer is not None:
			message = FogMessage.reading(value if value else [])
			toReceive = self.deserializer(message)
		else:
			toReceive = value
		self.receiving(toReceive)
		#TODO BZ Bluezero immediately calls set_value with the old value.
		#This does not allow for validation, filtration, transformation,
		#change detection, or ordered side-effects before broadcast
		#This exception "stops" that work from being performed.
		raise ValueError('Bluezero workaround to stop old/garbage value from being broadcasted')
