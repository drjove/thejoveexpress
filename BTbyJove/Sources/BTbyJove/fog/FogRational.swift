//
//  FogRational.swift
//  BTbyJove
//
//  Created by David Giovannini on 8/9/17.
//  Copyright © 2017 Software by Jove. All rights reserved.
//

import Foundation

public struct FogRational<T: FixedWidthInteger> : Equatable, FogExternalizable, CustomStringConvertible {
    public typealias ValueType = T
	public var num: T = 0
	public var den: T = 1
	
	public init(num: T = 0, den: T = 1) {
		self.num = num
		self.den = den
	}
	
	public init(ratio: Double) {
		self.num = T(ratio * Double(T.max))
		self.den = T.max
	}
	
	public init(fog data: Data, at cursor: inout Int) throws {
		self.num = try T(fog: data, at: &cursor)
		self.den = try T(fog: data, at: &cursor)
	}
	
	public var ratio: Double {
		Double(num) / Double(den)
	}

	public var description: String {
		return "\(num)/\(den)"
	}
	
	public static var fogSize: Int {
		return T.fogSize + T.fogSize
	}
	
	public var fogSize: Int {
		return Self.fogSize
	}
	
	public func write(fog data: inout Data) {
		self.num.write(fog: &data)
		self.den.write(fog: &data)
	}
	
	public static func ==(lhs: FogRational<T>, rhs: FogRational<T>) -> Bool {
		return lhs.num == rhs.num && lhs.den == rhs.den
	}
	
	public static func !=(lhs: FogRational<T>, rhs: FogRational<T>) -> Bool {
		return lhs.num != rhs.num || lhs.den != rhs.den
	}
}
