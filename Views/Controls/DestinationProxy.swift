//
//  DestinationProxy.swift
//  The Jove Express
//
//  Created by David Giovannini on 7/14/21.
//

import SwiftUI

struct DestinationProxy<Content: View>: View {
	@Binding var isActive: Bool
	let builder: ()->Content
	@State private var handle = Handle()
	
	class Handle {
		var content: Content?
	}
	
	init(_ isActive: Binding<Bool>, @ViewBuilder _ builder: @escaping ()->Content) {
		self._isActive = isActive
		self.builder = builder
	}
	
	private var content: Content {
		if handle.content == nil {
			handle.content = builder()
		}
		return handle.content!
	}
	
    var body: some View {
		if isActive {
			self.content
		}
		else {
			EmptyView()
		}
    }
}
