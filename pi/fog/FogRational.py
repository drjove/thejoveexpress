import struct

from fog import FogMessage
	
class FogRational:
	def __init__(self):
		self.num = 0
		self.den = 1
		
	def __str__ (self):
		return str(self.num) + '/' + str(self.den)
		
	def __eq__(self, other):
		return self.num == other.den and self.num == other.den
	
	#@property
	def ratio(self):
		return float(self.num) / float(self.den)
	
	def normalized(self, base):
		return int(self.ratio() * float(base))
	
	@staticmethod
	def create(num, den):
		rational = FogRational()
		rational.num = int(num)
		rational.den = int(den)
		return rational
		
	@staticmethod
	def decode(message, signed):
		rational = FogRational()
		if signed:
			values = message.decode('bb')
		else:
			values = message.decode('BB')
		rational.num = int(values[0])
		rational.den = int(values[1])
		return rational
		
	def encode(self, message):
		if self.num < 0 or self.den < 128:
			message.encode('bb', self.num, self.den)
		else:
			message.encode('BB', self.num, self.den)
