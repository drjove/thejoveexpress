//
//  LightsView.swift
//  The Jove Express
//
//  Created by David Giovannini on 7/9/21.
//

import SwiftUI
import BTbyJove

struct LightsView: View {
	@ObservedObject var calibration: BTSubject<LightsRational>
	@ObservedObject var power: BTSubject<LightCommand>
	@ObservedObject var state: BTSubject<Bool>
	@ObservedObject var sensed: BTSubject<LightsRational>
	
	static let gaugeConfig = GaugeConfig(
		unit: "Ambient",
		minMax: 0...256,
		scaleDivisions: 8,
		scaleSubDivisions: 4,
		ranges: [
			("Dark", GuageColor.darkGray),
			("Light", GuageColor.lightGray),
		],
		showControlMaker: false)
	
	init(_ lights: Lights) {
		self._calibration = ObservedObject(initialValue: lights.calibration)
		self._power = ObservedObject(initialValue: lights.power)
		self._state = ObservedObject(initialValue: lights.state)
		self._sensed = ObservedObject(initialValue: lights.ambient)
	}
	
    var body: some View {
        VStack(alignment: .center, spacing: 12) {
			Gauge(
				config: Self.gaugeConfig,
				value: sensed.feedback,
				marker: FogRational(ratio: 1.0),
				ranges: [
					Double(calibration.feedback.num),
					256.0],
				indicator: GuageImage(named: state.feedback ? "TorchOn" : "TorchOff")!)
					.aspectRatio(1, contentMode: .fit)
			Picker("", selection: $power.control) {
				ForEach(LightCommand.allCases, id: \.self) {
					Text($0.description)
				}
			}
			.pickerStyle(SegmentedPickerStyle())
			Text("Lights").multilineTextAlignment(.center)
			RationalSlider(hasNegative: false, value: $calibration.control, thumbColor: Color.gray, minTrackColor: Color.black, maxTrackColor: Color.white)
			Text("Ambient Threshold").multilineTextAlignment(.center)
        }
    }
}

struct LightsView_Previews: PreviewProvider {
    static var previews: some View {
		LightsView(Lights(broadcaster: NullBTBroadcaster()))
    }
}
