from bluezero import peripheral
from btProperty import BTProperty

from fog.HeartHealth import HeartHealth
from fog.FogMessage import FogMessage

class Heart:
	def __init__(self, peripheral, serviceIdx, killSwitch):
		self.beatIdx = 0
		self.killSwitch = killSwitch
		
		self.limitsProp = BTProperty(
			'01030000-0000-1000-8000-0080544A4578', HeartHealth(),
			HeartHealth.encode,
			HeartHealth.decode,
			saves=False)
		self.stopProp = BTProperty(
			'01030100-0000-1000-8000-0080544A4578', False,
			FogMessage.encodeBool,
			FogMessage.decodeBool,
			saves=False)
		self.beatProp = BTProperty(
			'01030200-0000-1000-8000-0080544A4578', 0,
			FogMessage.encodeUByte,
			None,
			saves=False)
		self.healthProp = BTProperty(
			'01030300-0000-1000-8000-0080544A4578', HeartHealth(),
			HeartHealth.encode,
			None,
			saves=False)
			
		self.limitsProp.activate(peripheral, serviceIdx, self.limitting)
		self.stopProp.activate(peripheral, serviceIdx, self.fullStop)
		self.beatProp.activate(peripheral, serviceIdx, None)
		self.healthProp.activate(peripheral, serviceIdx, None)
			
	def limitting(self, health):
		pass

	def fullStop(self, stopping):
		if stopping:
			self.killSwitch()

	def beat(self, interval):
		base = int(1.0 / interval)
		if self.beatIdx > 99:
			self.beatIdx = 0
		if ((self.beatIdx + base) % base) == 0:
			beatNumber = int(self.beatIdx / base)
			self.beatProp.value = beatNumber
		self.beatIdx += 1
			
	def senseHealth(self, health):
		self.healthProp.value = health

	def kill(self):
		pass
