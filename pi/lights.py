import time

from bluezero import peripheral
from hardware.PCA9685 import PCA9685Driver
from btProperty import BTProperty
from fog.FogMessage import FogMessage
from fog.FogRational import FogRational

class Lights:
	def __init__(self, peripheral, serviceIdx, hardware, port):
		self.hardware = hardware
		self.port = port
		
		#FUTURE: can be BT properties
		self.dim = 0
		self.bright = 100

		self.calibrationProp = BTProperty(
			'01020000-0000-1000-8000-0080544A4578', 128,
			lambda message, value: FogRational.create(value, 255).encode(message),
			lambda message: FogRational.decode(message, False).normalized(255),
			saves=True)
		
		self.powerProp = BTProperty(
			'01020100-0000-1000-8000-0080544A4578', -1,
			FogMessage.encodeByte,
			FogMessage.decodeByte,
			saves=True)
			
		power = self.powerProp.value
		if power == 0:
			self.sensedState = True
			state = False
		elif power == 1:
			self.sensedState = True
			state = True
		else:
			self.sensedState = False
			state = False
			
		self.stateProp = BTProperty(
			'01020200-0000-1000-8000-0080544A4578', state,
			FogMessage.encodeBool,
			None,
			saves=False)
			
		self.ambientProp = BTProperty(
			'01020300-0000-1000-8000-0080544A4578', 128,
			lambda message, value: FogRational.create(value, 255).encode(message),
			None,
			saves=False)

		self.hardware.run(self.port, state)
		
		self.calibrationProp.activate(peripheral, serviceIdx, self.calibrate)
		self.powerProp.activate(peripheral, serviceIdx, self.run)
		self.stateProp.activate(peripheral, serviceIdx, None)
		self.ambientProp.activate(peripheral, serviceIdx, None)

	def signalOn(self):
		state = self.stateProp.value
		self._run(not state)
		time.sleep(1.0)
		self._run(state)
		time.sleep(1.0)
		self._run(not state)
		time.sleep(1.0)
		self._run(state)
		
	def signalConnected(self):
		state = self.stateProp.value
		self._run(not state)
		time.sleep(0.5)
		self._run(state)
		time.sleep(0.5)
		self._run(not state)
		time.sleep(0.5)
		self._run(state)
		
	def signalDisconnected(self):
		self.signalConnected()
		
	def run(self, power):
		power = int(power)
		self.powerProp.value = power
		if power == 0:
			self._run(False)
		elif power == 1:
			self._run(True)
		elif self.ambientProp.value > self.calibrationProp.value:
			self._run(False)
		else:
			self._run(True)

	def kill(self):
		self._run(False)

	def calibrate(self, calibration):
		calibration = int(calibration)
		if calibration < 0:
			calibration = 0
		elif calibration > 255:
			calibration = 255
		self.calibrationProp.value = calibration
		self._react()
		
	def sensedAmbient(self, rgbc):
		ambient = rgbc.luminance()
		self.ambientProp.value = ambient
		self._react()
		
	def _react(self):
		if self.powerProp.value == -1:
			currentCalibration = self.calibrationProp.value
			currentAmbient = self.ambientProp.value
			if self.sensedState:
				currentState = self.stateProp.value
				if currentAmbient <= currentCalibration and currentState == False:
					self._run(True)
				elif currentAmbient > currentCalibration and currentState == True:
					self._run(False)
			else:
				if currentAmbient <= currentCalibration:
					self._run(True)
				elif currentAmbient > currentCalibration:
					self._run(False)
			self.sensedState = True
				
	def _run(self, state):
		self.hardware.run(self.port, self.bright if state else self.dim)
		self.stateProp.value = state
