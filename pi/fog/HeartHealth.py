import struct

from fog import FogMessage

#FUTURE: charging state and low voltage indicator

class HeartHealth:
	def __init__(self):
		self.cpuUsage = 0
		self.cpuTemp = 0
		self.internalTemp = 0
		self.internalPressure = 0
		
	def __str__ (self):
		return 'cpuUsage = %d cpuTemp = %dC internalTemp = %dC, internalPressure = %dhPa'%(self.cpuUsage, self.cpuTemp, self.internalTemp, self.internalPressure)
		
	@staticmethod
	def decode(message):
		health = HeartHealth()
		values = message.decode('>hhhh')
		health.cpuUsage = int(values[0])
		health.cpuTemp = int(values[1])
		health.internalTemp = int(values[2])
		health.internalPressure = int(values[3])
		return health
		
	@staticmethod
	def encode(message, self):
		message.encode('>hhhh', self.cpuUsage, self.cpuTemp, self.internalTemp, self.internalPressure)
