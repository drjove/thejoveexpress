//
//  Heart.swift
//  The Jove Express
//
//  Created by David Giovannini on 7/14/21.
//

import Foundation
import BTbyJove

public struct HeartHealth: FogExternalizable, Equatable, CustomStringConvertible {
	public let cpuUsage: Int16
	public let cpuTemp: Int16
	public let internalTemp: Int16
	public let internalPressure: Int16
	//FUTURE:
	// isCharging
	// cpuUnderVoltage

	init() {
		self.cpuUsage = 0
		self.cpuTemp = 0
		self.internalTemp = 0
		self.internalPressure = 0
	}

	public init(fog data: Data, at cursor: inout Int) throws {
		self.cpuUsage = try Int16(fog: data, at: &cursor)
		self.cpuTemp = try Int16(fog: data, at: &cursor)
		self.internalTemp = try Int16(fog: data, at: &cursor)
		self.internalPressure = try Int16(fog: data, at: &cursor)
	}
	
	public var fogSize: Int {
		cpuUsage.fogSize + cpuTemp.fogSize + internalTemp.fogSize + internalPressure.fogSize
	}

	public func write(fog data: inout Data) {
		self.cpuUsage.write(fog: &data)
		self.cpuTemp.write(fog: &data)
		self.internalTemp.write(fog: &data)
		self.internalPressure.write(fog: &data)
	}
	
	public var description: String {
		"\(cpuUsage)% \(cpuTemp)C \(internalTemp)C \(internalPressure)hPa"
	}
}

public struct Heart {
	public var limits: BTSubject<HeartHealth>
	public var beat: BTSubject<UInt8>
	public var fullStop: BTSubject<Bool>
	public var health: BTSubject<HeartHealth>

	public init(broadcaster: BTBroadcaster) {
		self.limits = BTSubject(
			BTCharacteristicIdentity(TrainCategory.heart, TrainPower.calibration),
			broadcaster,
			HeartHealth())
		self.fullStop = BTSubject(
			BTCharacteristicIdentity(TrainCategory.heart, TrainPower.power),
			broadcaster,
			false)
		self.beat = BTSubject(
			BTCharacteristicIdentity(TrainCategory.heart, TrainPower.state),
			broadcaster,
			0)
		self.health = BTSubject(
			BTCharacteristicIdentity(TrainCategory.heart, TrainPower.sensed),
			broadcaster,
			HeartHealth())
	}
	
	func reset() {
		limits.reset()
		fullStop.reset()
		beat.reset()
		health.reset()
	}
}
