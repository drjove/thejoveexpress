import threading
import time
from gpiozero import CPUTemperature
import os
import psutil

from fog.HeartHealth import HeartHealth

#FUTURE
# read motion
# better cpu% (PSUtil
# read under voltage indicator
# read is changing sensor

def getCPUuse():
	#p = psutil.Process(os.getpid())
	#print(p.cpu_percent())
	#return float(str(os.popen("top -n1 | awk '/Cpu\(s\):/ {print $2}'").readline().strip()))
	return 69

cpu = CPUTemperature()

from hardware.TCS34725 import TCS34725Driver
from hardware.LPS22HB import LPS22HBDriver

class SensorArray:
	def __init__(self, lightListener, beatListener, healthListener):
		self.lightListener = lightListener
		self.beatListener = beatListener
		self.healthListener = healthListener

	def run(self):
		self.running = True
		self.x = threading.Thread(target=self.thread_function, args=(1,))
		self.x.start()

	def kill(self):
		self.running = False
		self.x.join()

	#FUTURE: better scheduler
	
	def thread_function(self, name):
		color = TCS34725Driver()
		env = LPS22HBDriver()
		health = HeartHealth()
		
		time.sleep(max(color.requiredLoadTime(), env.requiredLoadTime()))
		
		i = 101
		while self.running:
			if i == 201:
				i = 101
			#Sleep a tenth of a second
			time.sleep(0.1)
			
			# Tenth of a second
			if (i % 1) == 0:
				self.beatListener(0.1)
				self.lightListener(color.read())
	
			# Half a second
			if (i % 5) == 0:
				pass
				
			healthCalled = False
			# One second
			if (i % 10) == 0:
				result = env.read()
				health.internalPressure = int(round(result[0]))
				health.internalTemp = int(round(result[1]))
				self.healthListener(health)
				healthCalled = True
			
			# 5 seconds
			if (i % 50) == 0:
				health.cpuTemp = int(round(cpu.temperature))
				health.cpuUsage = int(round(getCPUuse()))
				if healthCalled == False:
					self.healthListener(health)
			
			# 10 seconds
			if (i % 100) == 0:
				pass
			
			i += 1
