# System
import os
from gi.repository import GLib

# Bluezero modules
from bluezero import adapter
from bluezero import peripheral
from bluezero import device

# Local Modules
from btProperty import BTProperty
from hardware.PCA9685 import PCA9685Driver
from engine import Engine
from lights import Lights
from heart import Heart
from sensorArray import SensorArray
from camera import Camera
from display import Display

from preferences import Preferences
from fog.FogMessage import FogMessage

#No module scope code
#Gyro has sleeps in init (about 1 second)
#Display has sleeps in init (reset tiny)
#Pressure/Temp has sleeps in init (tiny)
#No on motor
#gp sensor .5
#Light 0.2

### GPIO
#Light Sensor 18, 17
#Motor I2C
#Temp/Pressure I2C
#Display RST = 26, DC = 24, BL = 16, SPI
#GYRO I2C
#gp senser I2C 0x48

class Train:
	def __init__(self, adapter):
		self.adapter = adapter
		print("Adatper", adapter.address, adapter.name)
		TRAIN_SERVICE_IDX = 1
		TRAIN_SERVICE = '00000000-0000-1000-8000-0080544A4578'
		
		Preferences.open("TheJoveExpress")
		
		self.nameProp = BTProperty(
			'01000000-0000-1000-8000-0080544A4578',
			adapter.name,
			FogMessage.encodeVarString,
			FogMessage.decodeVarString,
			saves=True)
		print("Alias", self.nameProp.value)

		self.peripheral = peripheral.Peripheral(adapter.address, local_name=self.nameProp.value)
		self.peripheral.add_service(srv_id=TRAIN_SERVICE_IDX, uuid=TRAIN_SERVICE, primary=True)

		self.peripheral.on_connect = self.on_connect
		self.peripheral.on_disconnect = self.on_disconnect
		
		self.nameProp.activate(self.peripheral, TRAIN_SERVICE_IDX, self._changeName)
		
		self.heart = Heart(self.peripheral, TRAIN_SERVICE_IDX, self.killSwitch)
		
		print("Initializing Hardware")

		servoDriver = PCA9685Driver()
		self.engine = Engine(self.peripheral, TRAIN_SERVICE_IDX, servoDriver, 0, True)
		self.lights = Lights(self.peripheral, TRAIN_SERVICE_IDX, servoDriver, 1)
		self.camera = Camera(self.peripheral, TRAIN_SERVICE_IDX)
		self.display = Display(self.peripheral, TRAIN_SERVICE_IDX)

		self.sensors = SensorArray(
			self.lights.sensedAmbient,
			self.heart.beat,
			self.heart.senseHealth)
		
	def _changeName(self, name):
		if len(name) > 0:
			self.adapter.alias = name
			self.nameProp.value = name

	def run(self):
		print("Running")
		try:
			self.sensors.run()
			self.camera.run()
			self.display.signalOn()
			self.lights.signalOn()
			self.peripheral.publish()
		except KeyboardInterrupt:
			self.killSwitch()
			
	def killSwitch(self):
		print("Kill 0")
		self._kill()

	def _kill(self):
		print("Kill 1")
		#self.display.kill()
		print("Kill 2")
		#self.sensors.kill()
		print("Kill 3")
		#self.camera.kill()
		print("Kill 4")
		#self.engine.kill()
		print("Kill 5")
		#self.lights.kill()
		print("Kill 6")
		#self.heart.kill()
		print("Kill 7")
		#Preferences.close()
		print("Kill 8")
		#self.adapter.quit()
		#TODO BZ
		print("Kill 9")
		sys.exit(0)

	def on_connect(self, ble_device: device.Device):
		print("Connected to " + str(ble_device.address))
		self.lights.signalConnected()

	def on_disconnect(self, adapter_address, device_address):
		print("Disconnected from " + device_address)
		self.lights.signalDisconnected()

import psutil
import logging

def main(adapter):
	for process in psutil.process_iter():
		cmdLine = process.cmdline()
		if len(cmdLine) > 1:
			if cmdLine[0] == 'python3':
				if cmdLine[1].endswith('train.py'):
					#print(process.cmdline(), process.pid, os.getpid())
					if process.pid != os.getpid():
						print("Killing", process.pid)
						os.kill(process.pid, 9)
	#TODO logging
	#logger = logging.getLogger('localGATT')
    #logger.setLevel(logging.DEBUG)
	#logging.basicConfig(level=logging.DEBUG)
    
	train = Train(adapter)
	train.run()
	
if __name__ == '__main__':
	print("Main")
	adapter = list(adapter.Adapter.available())[0]
	main(adapter)
