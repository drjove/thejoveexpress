//
//  BTClient.swift
//  The Jove Express
//
//  Created by David Giovannini on 6/30/21.
//

import Foundation
import Combine
import CoreBluetooth

public enum BTScanFilter {
	case no
	case yes(stop: Bool)
}

public class BTClient: ObservableObject {
	private let scanner: BTScanner
	private let services: [BTServiceIdentity]
	private let filter: (CBPeripheral)->BTScanFilter
	private var sink: Set<AnyCancellable> = []
	
	private var known: [UUID: BTDevice] = [:]{
		didSet {
			self.devices = self.known.values.sorted {
				$0.name < $1.name
			}
		}
	}
	
	@Published public private(set) var devices: [BTDevice] = []
	
	public init(services: [BTServiceIdentity], filter: @escaping (CBPeripheral)->BTScanFilter = {_ in .yes(stop: false)}) {
		self.scanner = BTScanner()
		self.services = services
		self.filter = filter
		scanner.delegate = self
	}
	
	@Published public var scanning: Bool = false {
		didSet {
			//print("BTClient Looking \(scanning)")
			if scanning {
				self.scanner.startScan(services: services)
			}
			else {
				self.scanner.stopScan()
			}
		}
	}
}

extension BTClient: BTScannerDelegate {
	public func peripheralDiscovered(_ peripheral: CBPeripheral, advertisementData: [String : Any], rssi RSSI: NSNumber) {
		let scanner = self.scanner
		print("BTClient found \(peripheral.identifier)")
		switch filter(peripheral) {
		case .no:
			break
		case .yes(let stop):
			if stop {
				scanner.stopScan()
			}
			self.known[peripheral.identifier] = self.create(peripheral)
		}
	}

	public func createDummy() -> BTDevice {
		self.create(nil)
	}

	private func create(_ peripheral: CBPeripheral?) -> BTDevice {
		let scanner = self.scanner
		return BTDevice(peripheral: peripheral, service: services.first!) {
			if let peripheral = peripheral {
				if $0 {
					scanner.connect(device: peripheral)
				}
				else {
					scanner.disconnect(device: peripheral)
				}
			}
		}
	}
	
	public func peripheralConnected(_ peripheral: CBPeripheral) {
		known[peripheral.identifier]?.peripheralConnected(peripheral)
	}
	
	public func peripheralConnectFailed(_ peripheral: CBPeripheral, _ error: Error?) {
		known[peripheral.identifier]?.peripheralConnectFailed(peripheral, error)
	}
	
	public func peripheralDisconnected(_ peripheral: CBPeripheral, _ error: Error?) {
		known[peripheral.identifier]?.peripheralDisconnected(peripheral, error)
	}
}
