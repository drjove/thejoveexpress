import shelve
import time
import os

from appdirs import *

class Preferences:
	@classmethod
	def open(cls, name):
		dirs = AppDirs(name, "Acme")
		dir = dirs.user_data_dir
		if not os.path.exists(dir):
			os.makedirs(dir)
		path = os.path.join(dir, "prefs")
		print("Prefs:" , path)
		cls.theShelf = shelve.open(path)
		cls.last_sync = None
	
	@classmethod
	def close(cls):
		cls.theShelf.close()
		
	@classmethod
	def get(cls, key, dft):
		#print("Prefs get:", key, dft)
		if key in cls.theShelf:
			#print("Prefs has:", key)
			value = cls.theShelf[key]
			#print("Prefs has:", value)
			if value is not None:
				#print("Prefs returning:", value)
				return value
		#print("Prefs defaulting:", key)
		value = dft()
		#print("Prefs defaulted:", value)
		if value is not None:
			#print("Prefs setting default:", value)
			cls.theShelf[key] = value
		#print("Prefs returning default:", value)
		return value
	
	@classmethod
	def set(cls, key, value):
		if value is not None:
			#print("setPref", key, value)
			cls.theShelf[key] = value
		elif key in cls.theShelf:
			#print("delPref", key)
			del d[key]
		Preferences._trySync()
		
	@classmethod
	def _trySync(cls):
		currentTime = time.time()
		if cls.last_sync is None:
			#print("_trySync first", currentTime)
			cls.last_sync = currentTime
			cls.theShelf.sync()
		elif currentTime - cls.last_sync > 3.0:
			#print("syncing")
			cls.last_sync = currentTime
			cls.theShelf.sync()
