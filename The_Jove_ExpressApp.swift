//
//  The_Jove_ExpressApp.swift
//  Shared
//
//  Created by David Giovannini on 6/30/21.
//

import SwiftUI
import BTbyJove

@main
struct The_Jove_ExpressApp: App {
	public static let service = BTServiceIdentity(name: "TJEx")

	@ObservedObject var client = BTClient(services: [Self.service])

	@SceneBuilder var body: some Scene {
		WindowGroup {
			TrainSelectorView(client: client)
		}
#if os(watchOS)
		WKNotificationScene(controller: NotificationController.self, category: "myCategory")
#endif
	}
}
