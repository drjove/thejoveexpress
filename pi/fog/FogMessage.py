import struct

class FogMessage():
	def __init__(self, data):
		self.data = data
		self.cursor = 0
		
	def __str__ (self):
		return str(self.cursor) + "." + str(len(self.data))
	
	@staticmethod
	def reading(data):
		message = FogMessage(bytes(data))
		return message
		
	@staticmethod
	def writing():
		message = FogMessage(bytearray(20))
		return message
		
	#@property
	def written(self):
		return self.data[:self.cursor]
		
	def encode(self, format, *values):
		struct.pack_into(format, self.data, self.cursor, *values)
		self.cursor += struct.calcsize(format)
	
	def decode(self, format):
		size = struct.calcsize(format)
		if self.cursor + size > len(self.data):
			return None
		t = struct.unpack_from(format, self.data, self.cursor)
		self.cursor += size
		return t
		
	def encodeUByte(self, value):
		self.encode('B', value)
		
	def encodeByte(self, value):
		self.encode('b', value)
		
	def encodeIP(self, address):
		a = address[0].split(".")
		i = list(map(int, a))
		self.encode('>BBBBHB', i[0], i[1], i[2], i[3], address[1], 0)
		
	def decodeUByte(self):
		return self.decode('B')[0]
		
	def decodeByte(self):
		return self.decode('b')[0]
		
	def encodeBool(self, value):
		toEncode = 0 if value == False else 1
		self.encode('B', toEncode)
		
	def decodeBool(self):
		value = self.decode('B')[0]
		value = False if value == 0 else True
		return value
		
	def encodeVarBytes(self, bytes):
		count = len(bytes)
		struct.pack_into('>B', self.data, self.cursor, count)
		self.cursor += 1
		struct.pack_into('{c}s'.format(c = count), self.data, self.cursor, bytes)
		self.cursor += count
		
	def encodeVarString(self, str):
		self.encodeVarBytes(str.encode())
		
	def decodeVarBytes(self):
		if self.cursor + 1 > len(self.data):
			return None
		t = struct.unpack_from('>B', self.data, self.cursor)
		self.cursor += 1
		count = t[0]
		if self.cursor + count > len(self.data):
			return None
		t = struct.unpack_from('{c}s'.format(c = count), self.data, self.cursor)
		self.cursor += count
		return t[0]
		
	def decodeVarString(self):
		bytes = self.decodeVarBytes()
		if bytes:
			return bytes.decode('utf-8')
		return None
