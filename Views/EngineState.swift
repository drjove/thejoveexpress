//
//  EngineState.swift
//  The Jove Express
//
//  Created by David Giovannini on 7/16/21.
//

import Foundation

#if os(iOS)
import UIKit
typealias GuageColor = UIColor
typealias GuageImage = UIImage
#elseif os(watchOS)
import UIKit
typealias GuageColor = UIColor
typealias GuageImage = UIImage
#else
typealias GuageColor = NSColor
typealias GuageImage = NSImage
#endif

public enum EngineState: Int8 {
	case reverse = -1
	case idle = 0
	case forward = 1

	init(power: Int8) {
		if power == 0 {
			self = .idle
		}
		else if power > 0 {
			self = .forward
		}
		else {
			self = .reverse
		}
	}
	
	var image: GuageImage {
		switch self {
		case .reverse:
			return GuageImage(named: "MotionReverse")!
		case .idle:
			return GuageImage(named: "MotionIdle")!
		case .forward:
			return GuageImage(named: "MotionForward")!
		}
	}
}
