//
//  EngineView.swift
//  The Jove Express
//
//  Created by David Giovannini on 7/9/21.
//

import SwiftUI
import Combine
import BTbyJove

struct EngineView: View {
	@ObservedObject var calibration: BTSubject<EngineRational>
	@ObservedObject var power: BTSubject<EngineRational>
	
	static let gaugeConfig = GaugeConfig(
		unit: "Power",
		minMax: -100.0...100.0,
		scaleDivisions: 8.0,
		scaleSubDivisions: 10.0,
		ranges: [
			("Reverse", GuageColor.red),
			("Idle", GuageColor.yellow),
			("Forward", GuageColor.green),
		],
		showControlMaker: true)
	
	init(_ engine: Engine) {
		self._calibration = ObservedObject(initialValue: engine.calibration)
		self._power = ObservedObject(initialValue: engine.power)
	}
	
    var body: some View {
        VStack(alignment: .center, spacing: 12) {
			Gauge(
				config: Self.gaugeConfig,
				value: power.feedback,
				marker: power.control,
				ranges: [
					-Double(self.calibration.feedback.num),
					 Double(self.calibration.feedback.num),
					 100.0],
				indicator: EngineState(power: power.feedback.num).image)
					.aspectRatio(1, contentMode: .fit)
			Scrubber(hasNegative: true, value: $power.control, thumbColor: Color.gray, minTrackColor: Color.red, maxTrackColor: Color.green)
			Text("Speed").multilineTextAlignment(.center)
			RationalSlider(hasNegative: false, value: $calibration.control, thumbColor: Color.gray, minTrackColor: Color.yellow, maxTrackColor: Color.red)
			Text("Idle Threshold").multilineTextAlignment(.center)
        }
    }
}

struct EngineView_Previews: PreviewProvider {
    static var previews: some View {
		EngineView(Engine(broadcaster: NullBTBroadcaster()))
    }
}
