//
//  BTServiceIdentity.swift
//  The Jove Express
//
//  Created by David Giovannini on 7/4/21.
//

import Foundation
import CoreBluetooth

public protocol BTComponent: CustomStringConvertible {
	var rawValue: UInt8 { get }
}

public extension BTComponent {
	var bitValue: UInt32 {
		UInt32(rawValue) << 24
	}
	
	var description: String {
		rawValue.description
	}
}

public struct MainComponent: BTComponent {
	public let rawValue: UInt8 = 1
	public init() {}
}

public protocol BTCategory: CustomStringConvertible {
	var rawValue: UInt8 { get }
}

public extension BTCategory {
	var bitValue: UInt32 {
		UInt32(rawValue) << 16
	}
	
	var description: String {
		rawValue.description
	}
}

public protocol BTSubCategory: CustomStringConvertible {
	var rawValue: UInt8 { get }
}

public extension BTSubCategory {
	var bitValue: UInt32 {
		UInt32(rawValue) << 8
	}
	
	var description: String {
		rawValue.description
	}
}

public enum BTChannel: UInt8,  CustomStringConvertible {
	case property = 0
	case control = 1
	case feedback = 2
	
	var bitValue: UInt32 {
		UInt32(rawValue) << 0
	}
	
	public var description: String {
		switch self {
		case .property:
			return "P"
		case .control:
			return "C"
		case .feedback:
			return "F"
		}
	}
}

public struct BTCharacteristicIdentity: Hashable, CustomStringConvertible {
	public let component: BTComponent
	public let category: BTCategory
	public let subCategory: BTSubCategory
	public let channel: BTChannel
	public let bitValue: UInt32
	
	public init(component: BTComponent = MainComponent(), _ category: BTCategory, _ subCategory: BTSubCategory, _ channel: BTChannel = .property) {
		self.component = component
		self.category = category
		self.subCategory = subCategory
		self.channel = channel
		self.bitValue = (component.bitValue | category.bitValue | subCategory.bitValue | channel.bitValue).bigEndian
	}
	
	public func apply(channel: BTChannel) -> BTCharacteristicIdentity {
		BTCharacteristicIdentity(category, subCategory, channel)
	}
	
	public static func == (lhs: BTCharacteristicIdentity, rhs: BTCharacteristicIdentity) -> Bool {
		rhs.bitValue == lhs.bitValue
	}
	
	public func hash(into hasher: inout Hasher) {
		bitValue.hash(into: &hasher)
	}
	
	public var description: String {
		"\(self.category).\(self.subCategory)[\(self.channel)]"
	}
}

public struct BTServiceIdentity: CustomStringConvertible {
	private static let base: Data = CBUUID(string: "00000000-0000-1000-8000-008000000000").data
	public let name: String
	private let mine: Data

	public init(name: String) {
		self.name = name
		var data = Self.base
		let normalized = name.prefix(4).padding(toLength: 4, withPad: " ", startingAt: 0).data(using: .ascii)!
		data.replaceSubrange(12...15, with: normalized)
		self.mine = data
	}
	
	public var identifer: CBUUID {
		CBUUID(data: mine)
	}
	
	public var description: String {
		name
	}

	public func characteristic(characteristic: BTCharacteristicIdentity) -> CBUUID {
		var data = mine
		let code = characteristic.bitValue
		withUnsafePointer(to: code) { address in
			data.replaceSubrange(0..<4, with: address, count: 4)
		}
		return CBUUID(data: data)
	}
}
