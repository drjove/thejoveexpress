//
//  TrainView.swift
//  The Jove Express Extension
//
//  Created by David Giovannini on 7/15/21.
//

import SwiftUI
import BTbyJove

struct TrainView: View {
	@ObservedObject var train: Train
	
	@ObservedObject var name: BTSubject<String>
	@ObservedObject var lightsPower: BTSubject<LightCommand>
	@ObservedObject var lightsState: BTSubject<Bool>
	@ObservedObject var enginePower: BTSubject<EngineRational>
	
    @State private var crown = 0.0
    @State private var lastValue = 0.0
    
    init(train: Train) {
		self.train = train
		self.name = train.name
		self.lightsPower = train.lights.power
		self.lightsState = train.lights.state
		self.enginePower = train.engine.power
	}
    
    var body: some View {
		ZStack {
			Image("Metal")
				.resizable()
				.cornerRadius(8)
			if train.state == .connected {
				VStack(spacing: 8) {
					Spacer()
					HStack {
						Image(lightsState.feedback ? "TorchOn" : "TorchOff")
							.resizable()
							.aspectRatio(contentMode: .fit)
						Image(uiImage: EngineState(power: enginePower.feedback.num).image)
							.resizable()
							.aspectRatio(contentMode: .fit)
					}
					Text(enginePower.control.num.description).font(.title)
					HStack(spacing: 0) {
						ForEach(LightCommand.allCases, id: \.self) { option in
							Button {
								self.lightsPower.control = option
							} label: {
								Text(option.symbol)
							}
							.border(
								self.lightsPower.control == option ?
									Color.black : Color.clear
								, width: 3.0)
							.cornerRadius(4.0)
						}
					}
					//Picker("", selection: $lightsPower.control) {
					Spacer()
				}
				.focusable(true).digitalCrownRotation($crown)
				.onChange(of: self.crown) { amount in
					let delta = amount < 0 ? -1.0 : 1.0
					let base = 100.0
					let ratio = enginePower.control.ratio
					let normalized = ratio * base
					var changed = normalized + delta
					if changed	> 100.0 {
						changed = 100.0
					}
					if changed	< -100.0 {
						changed = -100.0
					}
					let power = EngineRational(num: Int8(changed.rounded()), den: 100)
					enginePower.control = power
				}
			}
			else {
				Button(action: {
					train.connect()
				}) {
					train.state.image
				}
			}
		}
		.ignoresSafeArea(edges: .bottom)
		.navigationTitle(name.feedback)
		.onAppear {
			train.connect()
		}
		.onDisappear() {
			train.disconnect()
		}
	}
}

struct ContentView_Previews: PreviewProvider {
	static var previews: some View {
		TrainView(train: Train(device: BTDevice(peripheral: nil, service: The_Jove_ExpressApp.service, connection: {_ in})))
	}
}
