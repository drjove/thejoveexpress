//
//  Gauge.swift
//  The Jove Express
//
//  Created by David Giovannini on 7/9/21.
//

import SwiftUI
import BTbyJove

struct GaugeConfig {
	let unit: String
	let minMax: ClosedRange<Float>
	let scaleDivisions: CGFloat
	let scaleSubDivisions: CGFloat
	let ranges: [(String, GuageColor)]
	let showControlMaker: Bool
}

#if os(iOS)
struct Gauge<T: FixedWidthInteger>: UIViewRepresentable {
	let config: GaugeConfig
	let value: FogRational<T>
	let marker: FogRational<T>
	let ranges: [Double]
	let indicator: GuageImage?
	
	func makeUIView(context: Self.Context) -> WMGaugeView {
		let v = WMGaugeView(frame: CGRect.zero)
		
		v.backgroundColor = UIColor.clear
		v.scaleDivisionColor = UIColor.white
		v.showUnitOfMeasurement = true
		v.showRangeLabels = true
		v.scaleIgnoreRangeColors = true
		v.rangeLabelsFontColor = UIColor(red: 0.26663362979999999, green: 0.26668566469999999, blue: 0.26663035149999997, alpha: 1.0)
		
		v.minValue = config.minMax.lowerBound
		v.maxValue = config.minMax.upperBound
		v.scaleDivisions = config.scaleDivisions
		v.scaleSubdivisions = config.scaleSubDivisions
		v.unitOfMeasurement = config.unit
		v.rangeLabels = config.ranges.map(\.0)
		v.rangeColors = config.ranges.map(\.1)
		
		if config.showControlMaker == false {
			v.controlMarkerDiameter = 0.0
		}
		
		return v
	}
	
	func updateUIView(_ uiView: WMGaugeView, context: Context) {
		uiView.marker = Float(marker.ratio) * uiView.maxValue
		uiView.setValue(Float(value.ratio) * uiView.maxValue, animated: false)
		uiView.indicator = indicator
		uiView.rangeValues = ranges.map({NSNumber(value: $0)})
	}
}

#else

struct Gauge<T: FixedWidthInteger>: View {
	let config: GaugeConfig
	let value: FogRational<T>
	let marker: FogRational<T>
	let ranges: [Double]
	let indicator: GuageImage?
	
	var body: Text {
		Text("\(value.description) \(ranges.description)")
	}
}
#endif

//struct Gauge_Previews: PreviewProvider {
//    static var previews: some View {
//		Gauge(value: .constant(EngineRational(num: 0, den: 100)), range: .constant([]))
//    }
//}
