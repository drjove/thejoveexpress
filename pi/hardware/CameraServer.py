# Web streaming example
# Source code from the official PiCamera package
# http://picamera.readthedocs.io/en/latest/recipes2.html#web-streaming

import io
import picamera
import logging
import socketserver
from threading import Condition
from http import server
import threading
import time
import socket

class StreamingCameraOutput(object):
	def __init__(self):
		self.frame = None
		self.buffer = io.BytesIO()
		self.condition = Condition()

	def write(self, buf):
		if buf.startswith(b'\xff\xd8'):
			self.buffer.truncate()
			with self.condition:
				self.frame = self.buffer.getvalue()
				self.condition.notify_all()
			self.buffer.seek(0)
		return self.buffer.write(buf)

class StreamingCameraRequestHandler(server.BaseHTTPRequestHandler):
	def __init__(self, page, output, *args):
		self.p = page.encode('utf-8')
		self.output = output
		super().__init__(*args)

	def do_GET(self):
		if self.path == '/':
			self.send_response(301)
			self.send_header('Location', '/index.html')
			self.end_headers()
		elif self.path == '/index.html':
			content = self.p
			self.send_response(200)
			self.send_header('Content-Type', 'text/html')
			self.send_header('Content-Length', len(content))
			self.end_headers()
			self.wfile.write(content)
		elif self.path == '/stream.mjpg':
			self.send_response(200)
			self.send_header('Age', 0)
			self.send_header('Cache-Control', 'no-cache, private')
			self.send_header('Pragma', 'no-cache')
			self.send_header('Content-Type', 'multipart/x-mixed-replace; boundary=FRAME')
			self.end_headers()
			try:
				while True:
					with self.output.condition:
						self.output.condition.wait()
						frame = self.output.frame
					self.wfile.write(b'--FRAME\r\n')
					self.send_header('Content-Type', 'image/jpeg')
					self.send_header('Content-Length', len(frame))
					self.end_headers()
					self.wfile.write(frame)
					self.wfile.write(b'\r\n')
			except Exception as e:
				pass
				#logging.warning(
				#    'Removed streaming client %s: %s',
				#    self.client_address, str(e))
		else:
			self.send_error(404)
			self.end_headers()

class StreamingCameraServer(socketserver.ThreadingMixIn, server.HTTPServer):
	allow_reuse_address = True
	daemon_threads = True
    
class CameraServer:
	def __init__(self, port=8081, rotation=0, resolution=(640, 480), frameRate=24):
		self.port = port
		self.x = None
		self.isPaused=True
		page="""\
		<html><body>
		<img src="stream.mjpg" width="%d" height="%d">
		</body></html>
		"""%(resolution[0], resolution[1])
		try:
			self.camera = picamera.PiCamera(resolution=resolution, framerate=frameRate)
			self.camera.rotation = rotation * 90
		except Exception as e:
			self.camera = None
			page="""\
			<html><body>
			No Camera
			</body></html>
			"""
		self.output = StreamingCameraOutput()
		def handler(*args):
			return StreamingCameraRequestHandler(page, self.output, *args)
		self.server = StreamingCameraServer(('', port), handler)

	@property
	def paused(self):
		return self.isPaused

	@paused.setter
	def paused(self, newValue):
		if self.camera is not None:
			self.isPaused=newValue
			if newValue:
				self.camera.stop_recording()
			else:
				self.camera.start_recording(self.output, format='mjpeg')

	@property
	def frameRate(self, frameRate):
		if self.camera is not None:
			return self.camera.framerate
		else:
			return 6

	@frameRate.setter
	def frameRate(self, newValue):
		if self.camera is not None:
			if not self.isPaused:
				self.camera.stop_recording()
			self.camera.framerate = newValue
			if not self.isPaused:
				self.camera.start_recording(self.output, format='mjpeg')

	def lowLighting(self, low):
		pass

	def run(self, cameraOn):
		self.isPaused = not cameraOn
		if cameraOn:
			if self.camera is not None:
				self.camera.start_recording(self.output, format='mjpeg')
		self.x = threading.Thread(target=self._startServer, args=(1,))
		self.x.start()

	def kill(self):
		self.server.shutdown()
		self.x = None

	def join(self):
		self.x.join()
		self.x = None

	def _startServer(self, name):
		try:
			self.server.serve_forever()
		finally:
			if self.camera is not None:
				self.camera.close()
			self.server = None

if __name__ == '__main__':
	server = CameraServer()
	server.run(True)
	server.frameRate = 6
	server.join()
