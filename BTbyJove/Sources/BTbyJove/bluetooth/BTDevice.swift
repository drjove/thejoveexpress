//
//  BTDevice.swift
//  The Jove Express
//
//  Created by David Giovannini on 6/30/21.
//

import Foundation
import Combine
import CoreBluetooth

public class BTDevice: NSObject, ObservableObject, BTControl, Identifiable {
	private let peripheral : CBPeripheral?
	private let service: BTServiceIdentity
	private let connection: (Bool)->()
	private var characteristics: [String: CBCharacteristic] = [:]
	private var notifyActivating: Set<String> = []
	private var distribute: [CombineIdentifier: [String:(Data)->()]] = [:]
	private var cachedWrites: [BTCharacteristicIdentity: (Data, ((BTBroadcasterWriteResponse)->())?)] = [:]
	private var confirmingWrites: [String: (BTBroadcasterWriteResponse)->()] = [:]
	
	@Published public var connected: Bool = false
	
	public init(peripheral : CBPeripheral?, service: BTServiceIdentity, connection: @escaping (Bool)->()) {
		self.peripheral = peripheral
		self.service = service
		self.connection = connection
		super.init()
		peripheral?.delegate = self
	}
	
	deinit {
	}
	
	public var name: String {
		self.peripheral?.name ?? service.name
	}

	public func connect() {
		print("BTDevice connecting: \(service.identifer.uuidString)")
		self.notifyActivating.removeAll()
		if peripheral == nil {
			connected = true
		}
		else {
			self.connection(true)
		}
	}

	public func disconnect() {
		self.notifyActivating.removeAll()
		if peripheral == nil {
			connected = false
		}
		else {
			self.connection(false)
		}
	}
}

extension BTDevice: BTBroadcaster {
	public func send(data: Data, to value: BTCharacteristicIdentity, confirmed: ((BTBroadcasterWriteResponse)->())?) {
		////if connected {
		if let peripheral = peripheral {
			let identity = service.characteristic(characteristic: value).uuidString
			if let cb = self.characteristics[identity] {
				//print("BTDevice write: \(identity) -> \(data.fogHexDescription)")
				if let confirmed = confirmed, cb.properties.contains(.write) {
					//FUTURE: test
					confirmingWrites[identity] = confirmed
					peripheral.writeValue(data, for: cb, type: .withResponse)
				}
				else {
					var completion: ((BTBroadcasterWriteResponse)->())?
					let old = cachedWrites.removeValue(forKey: value)
					old?.1?(.notSent)
					if peripheral.canSendWriteWithoutResponse {
						completion = confirmed
					}
					else {
						//print("BTDevice write failed: \(identity) -> \(data.fogHexDescription)")
						cachedWrites[value] = (data, confirmed)
					}
					peripheral.writeValue(data, for: cb, type: .withoutResponse)
					completion?(.sentOnly)
				}
			}
		////}
		}
	}
	
	public func read(value: BTCharacteristicIdentity) -> Data? {
		let identity = service.characteristic(characteristic: value).uuidString
		if let cb = self.characteristics[identity] {
			return cb.value
		}
		return nil
	}
	
	public func request(value: BTCharacteristicIdentity) {
		//if connected {
		if let peripheral = peripheral {
			let identity = service.characteristic(characteristic: value).uuidString
			if let tx = self.characteristics[identity] {
				peripheral.readValue(for: tx)
			}
		}
		//}
	}
	
	public func sink(id: CombineIdentifier, to characteristic: BTCharacteristicIdentity, with: @escaping (Data) -> ()) -> AnyCancellable {
		let identifier = self.service.characteristic(characteristic: characteristic).uuidString
		distribute[id, default: [:]][identifier] = with
		print("BTDevice sink: \(id) to \(characteristic) as \(identifier)")

		if let peripheral = peripheral {
			let cb = characteristics[identifier]
			if let cb = cb {
				if cb.properties.contains(.notify) {
					if notifyActivating.contains(identifier) == false {
						print("    make notify")
						notifyActivating.insert(identifier)
						peripheral.setNotifyValue(true, for: cb)
					}
				}
				// request read?
			}
		}
		return AnyCancellable({self.distribute.removeValue(forKey: id)})
	}
}

extension BTDevice: CBPeripheralDelegate {
	public func peripheralConnected(_ peripheral: CBPeripheral) {
		//print("BTDevice connect: \(peripheral.identifier)")
		peripheral.discoverServices([service.identifer])
	}
	
	public func peripheralConnectFailed(_ peripheral: CBPeripheral, _ error: Error?) {
		//print("BTDevice connect failed: \(peripheral.identifier) \(error?.localizedDescription ?? "")")
		self.connected = false
	}
	
	public func peripheralDisconnected(_ peripheral: CBPeripheral, _ error: Error?) {
		//print("BTDevice disconnected: \(peripheral.identifier) \(error?.localizedDescription ?? "")")
		//FUTURE: do these auto error out?
//		confirmingWrites.forEach {
//			$0.value(.error(?))
//		}
		cachedWrites.forEach {
			$0.value.1?(.notSent)
		}
		self.connected = false
	}
	
	public func peripheral(_ peripheral: CBPeripheral, didDiscoverServices error: Error?) {
		//print("BTDevice services: \(peripheral.identifier) \(error?.localizedDescription ?? "")")
		for service in peripheral.services ?? [] {
			peripheral.discoverCharacteristics(nil, for: service)
			//print("    \(self.service.description) as \(service.uuid)")
		}
	}
	
	public func peripheral(_ peripheral: CBPeripheral, didDiscoverCharacteristicsFor service: CBService, error: Error?) {
		//print("BTDevice characteristics: \(service.uuid.uuidString) \(error?.localizedDescription ?? "")")
		for characteristic in service.characteristics ?? [] {
			let identity = characteristic.uuid.uuidString
			//print("    \(identity)")
			self.characteristics[identity] = characteristic
			if let data = characteristic.value, !data.isEmpty {
				//print("    initial value \(data.fogHexFormat())")
				self.distribute.forEach {
					if let route = $1[identity] {
						//print ("    to \($0)")
						route(data)
					}
				}
			}
			if characteristic.properties.contains(.notify) {
				//print("    Can Notify")
				if notifyActivating.contains(identity) == false {
					let atLeast1Listener = self.distribute.firstIndex { (key, value) in
						value[identity] != nil
					}
					if atLeast1Listener != nil {
						//print("    make notify")
						notifyActivating.insert(identity)
						peripheral.setNotifyValue(true, for: characteristic)
					}
				}
			}
			peripheral.discoverDescriptors(for: characteristic)
		}
		self.connected = true
	}
	
	public func peripheral(_ peripheral: CBPeripheral, didDiscoverDescriptorsFor characteristic: CBCharacteristic, error: Error?) {
		//print("BTDevice descriptors: \(characteristic.uuid.uuidString) \(error?.localizedDescription ?? "")")
		//for descriptor in characteristic.descriptors ?? [] {
			//print("    \(descriptor.value.debugDescription)")
		//}
	}
	
	public func peripheral(_ peripheral: CBPeripheral, didUpdateNotificationStateFor characteristic: CBCharacteristic, error: Error?) {
		//print("BTDevice notify state: \(characteristic.uuid.uuidString) \(error?.localizedDescription ?? "")")
		if (characteristic.isNotifying) {
			//print ("    is on")
			if characteristic.value?.isEmpty ?? true {
				if characteristic.properties.contains(.read) {
					//print ("    read request for empty data")
					peripheral.readValue(for: characteristic)
				}
			}
		}
	}
	
	public func peripheral(_ peripheral: CBPeripheral, didUpdateValueFor characteristic: CBCharacteristic, error: Error?) {
		let identifier = characteristic.uuid.uuidString
		//print("BTDevice update: \(identifier) \(characteristic.value?.fogHexFormat() ?? "nil") \(error?.localizedDescription ?? "")")
		if let data = characteristic.value, !data.isEmpty {
			self.distribute.forEach {
				if let route = $1[identifier] {
					//print ("    to \($0)")
					route(data)
				}
			}
		}
	}

	public func peripheralIsReady(toSendWriteWithoutResponse peripheral: CBPeripheral) {
		let values = cachedWrites
		cachedWrites.removeAll()
		//print("toSendWriteWithoutResponse \(peripheral.identifier) \(values.count)")
		values.forEach {
			self.send(data: $0.value.0, to: $0.key, confirmed: $0.value.1)
		}
	}

	public func peripheral(_ peripheral: CBPeripheral, didWriteValueFor characteristic: CBCharacteristic, error: Error?) {
		//print("didWriteValueFor \(characteristic.uuid) \(error != nil)")
		if let error = error {
			confirmingWrites.removeValue(forKey: characteristic.uuid.uuidString)?(.error(error))
		}
		else {
			confirmingWrites.removeValue(forKey: characteristic.uuid.uuidString)?(.reponseReceived)
		}
	}
	
	public func peripheral(_ peripheral: CBPeripheral, didModifyServices invalidatedServices: [CBService]) {
		//print("didModifyServices \(peripheral.identifier)")
		//for service in invalidatedServices {
		//	//print("        Invalidate: \(service.uuid)")
		//}
	}
	
	public func peripheral(_ peripheral: CBPeripheral, didDiscoverIncludedServicesFor service: CBService, error: Error?) {
		//print("didDiscoverIncludedServicesFor \(peripheral.identifier) \(error?.localizedDescription ?? "")")
	}
	
	public func peripheral(_ peripheral: CBPeripheral, didWriteValueFor descriptor: CBDescriptor, error: Error?) {
		//print("didWriteValueFor \(descriptor.description) \(error != nil)")
	}
	
	public func peripheralDidUpdateName(_ peripheral: CBPeripheral) {
		//print("peripheralDidUpdateName \(peripheral.identifier)")
	}
	
	public func peripheralDidUpdateRSSI(_ peripheral: CBPeripheral, error: Error?) {
		//print("peripheralDidUpdateRSSI \(peripheral.identifier) \(error?.localizedDescription ?? "")")
	}
	
	public func peripheral(_ peripheral: CBPeripheral, didReadRSSI RSSI: NSNumber, error: Error?) {
		//print("didReadRSSI \(peripheral.identifier) \(error?.localizedDescription ?? "")")
	}
	
	public func peripheral(_ peripheral: CBPeripheral, didOpen channel: CBL2CAPChannel?, error: Error?) {
		//print("didOpen \(peripheral.identifier) \(error?.localizedDescription ?? "")")
	}
}
