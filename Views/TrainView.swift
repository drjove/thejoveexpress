//
//  TrainView.swift
//  The Jove Express
//
//  Created by David Giovannini on 7/9/21.
//

import SwiftUI
import BTbyJove

struct TrainView: View {
	@ObservedObject var train: Train
	
	init(train: Train) {
		self.train = train
	}
	
    var body: some View {
		ZStack {
			Image("Metal")
				.resizable()
				.ignoresSafeArea()
			VStack {
				HStack(alignment: .top, spacing: 8) {
					EngineView(train.engine)
					LightsView(train.lights)
				}.fixedSize(horizontal: false, vertical: true)
				HStack(alignment: .top, spacing: 8) {
					MotionView()
					CameraView(train.camera, connected: train.state == .connected)
				}.fixedSize(horizontal: false, vertical: true)
				HStack(alignment: .top, spacing: 8) {
					Button("Display") {
					}
					Button("Sound") {
					}
				}.fixedSize(horizontal: false, vertical: true)
				Spacer()
			}
			.padding(EdgeInsets(top: 5, leading: 5, bottom: 5, trailing: 5))
		}
		.trainToolbar(train: train)
		.onAppear {
			train.connect()
		}
		.onDisappear() {
			train.disconnect()
		}
	}
}

//TODO: SwifTUI still creating empty navbar
//TODO: SwifTUI does not respect safe area in landscape
struct TrainToolbar: ViewModifier {
	@ObservedObject var train: Train
	@ObservedObject var name: BTSubject<String>
	@State private var editTitle = false

	init(train: Train) {
		self.train = train
		self.name = train.name
	}

	func body(content: Content) -> some View {
		content.toolbar {
			ToolbarItem(placement: .principal) {
				VStack {
					Text(name.feedback).font(.title)
					HealthView(health: train.heart.health).font(.subheadline)
				}
				.onTapGesture {
					self.editTitle.toggle()
				}
			}
			ToolbarItem(placement: .navigationBarTrailing) {
				ConnectionView(train: train)
			}
		}
		.alert(isPresented: self.$editTitle, TextFieldAlert(
			   title: "Name", placeholder: name.feedback, action: { result in
			   if let result = result, result.isEmpty == false {
				   name.control = result
			   }
		   }))
	}
}

extension View {
	func trainToolbar(train: Train) -> some View {
		modifier(TrainToolbar(train: train))
	}
}

struct TrainView_Previews: PreviewProvider {
    static var previews: some View {
		TrainView(train: Train(device: BTDevice(peripheral: nil, service: The_Jove_ExpressApp.service, connection: {_ in})))
    }
}
