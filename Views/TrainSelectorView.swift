//
//  TrainSelectorView.swift
//  The Jove Express
//
//  Created by David Giovannini on 7/9/21.
//

import SwiftUI
import BTbyJove

struct TrainSelectorView: View {
	@ObservedObject var client: BTClient
	@State private var isActive: Bool = false
	@State private var dummy: BTDevice
	
	init(client: BTClient) {
		self.client = client
		self._dummy = State(initialValue: client.createDummy())
	}
	
	var devices: [BTDevice] {
		#if targetEnvironment(simulator)
			[dummy]
		#else
			client.devices
		#endif
	}
	
	var body: some View {
		NavigationView {
			VStack {
				List(self.devices) { element in
					//TODO this behaves strangely
					NavigationLink(
						destination: DestinationProxy($isActive) {
							TrainView(train: Train(device: element))
						},
						isActive: $isActive,
						label: {Text(element.name)})
				}
				.onChange(of: isActive) {
					if $0 {
						self.client.scanning = false
					}
				}
				.onAppear() {
					client.scanning = true
				}
			}
			.navigationTitle("Discovery")
		}
	}
}
