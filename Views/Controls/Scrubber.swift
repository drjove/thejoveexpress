//
//  Scrubber.swift
//  The Jove Express
//
//  Created by David Giovannini on 7/9/21.
//

import SwiftUI
import BTbyJove

#if os(iOS)
import UIKit
typealias Scrubber = RationalSlider
/*
struct Scrubber<T: FixedWidthInteger>: UIViewRepresentable {
	@Binding var value: FogRational<T>
	
	func makeUIView(context: Context) -> ScrubControl {
		let v = ScrubControl(frame: CGRect.zero)
		v.backgroundColor = UIColor.clear
		v.minimumValue = -1.0
		v.maximumValue = 1.0
		v.cursorColor = UIColor.red
		v.tickBackground = UIColor(red: 133.0/255.0, green: 131.0/255.0, blue: 132.0/255.0, alpha: 0.63)
		v.tickColor = UIColor.white
		v.cursorThickness = 5
		v.velocityAdjustWidthFactor = 40
		
		v.normValue = Float(value.ratio)
		v.action = {
			self.value = FogRational(ratio: $0)
		}
		return v
	}
	
	func updateUIView(_ uiView: ScrubControl, context: Context) {
		uiView.normValue = Float(value.ratio)
	}
}
*/
#else
typealias Scrubber = RationalSlider
#endif

struct RationalSlider<T: FixedWidthInteger>: View {
	let hasNegative: Bool

	@Binding var value: FogRational<T>
	let thumbColor: Color?
	let minTrackColor: Color?
	let maxTrackColor: Color?

	init(hasNegative: Bool, value: Binding<FogRational<T>>, thumbColor: Color? = nil, minTrackColor: Color? = nil, maxTrackColor: Color? = nil) {
		self.hasNegative = hasNegative
		self._value = value
		self.thumbColor = thumbColor
		self.minTrackColor = minTrackColor
		self.maxTrackColor = maxTrackColor
	}

	var body: some View {
		SwiftUISlider(value: Binding<Double>(
			get: {
				if hasNegative == false {
					return value.ratio
				}
				else {
					return (value.ratio + 1.0) / 2.0
				}
			},
			set: { newValue in
				if hasNegative == false {
					self.value = FogRational(ratio: newValue)
				}
				else {
					self.value = FogRational(ratio: (newValue * 2.0) - 1.0)
				}
			}
		), thumbColor: self.thumbColor, minTrackColor: self.minTrackColor, maxTrackColor: self.maxTrackColor)
	}
}

struct SwiftUISlider: UIViewRepresentable {

  final class Coordinator: NSObject {
	// The class property value is a binding: It’s a reference to the SwiftUISlider
	// value, which receives a reference to a @State variable value in ContentView.
	var value: Binding<Double>

	// Create the binding when you initialize the Coordinator
	init(value: Binding<Double>) {
	  self.value = value
	}

	// Create a valueChanged(_:) action
	@objc func valueChanged(_ sender: UISlider) {
	  self.value.wrappedValue = Double(sender.value)
	}
  }

  let thumbColor: UIColor?
	let minTrackColor: UIColor?
	let maxTrackColor: UIColor?

  @Binding var value: Double

	init(value: Binding<Double>, thumbColor: Color? = nil, minTrackColor: Color? = nil, maxTrackColor: Color? = nil) {
		self._value = value
		self.thumbColor = thumbColor != nil ? UIColor(thumbColor!) : nil
		self.minTrackColor = minTrackColor != nil ? UIColor(minTrackColor!) : nil
		self.maxTrackColor = maxTrackColor != nil ? UIColor(maxTrackColor!) : nil
	}

  func makeUIView(context: Context) -> UISlider {
	let slider = UISlider(frame: .zero)
	slider.thumbTintColor = thumbColor
	slider.minimumTrackTintColor = minTrackColor
	slider.maximumTrackTintColor = maxTrackColor
	slider.value = Float(value)

	slider.addTarget(
	  context.coordinator,
	  action: #selector(Coordinator.valueChanged(_:)),
	  for: .valueChanged
	)

	return slider
  }

  func updateUIView(_ uiView: UISlider, context: Context) {
	// Coordinating data between UIView and SwiftUI view
	uiView.value = Float(self.value)
  }

  func makeCoordinator() -> SwiftUISlider.Coordinator {
	Coordinator(value: $value)
  }
}

struct Scrubber_Previews: PreviewProvider {
    static var previews: some View {
		Scrubber(hasNegative: true, value: .constant(FogRational(num: 3, den: 4)))
    }
}
