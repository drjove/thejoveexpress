# README #

The following are the steps I used to fully install and configure the Raspberry Pi.

I used the *Raspberry Pi Imager* available from <https://www.raspberrypi.org/software/>
Since disk space is not an issue with modern SD Cards, I'd suggest doing the full Raspberry Pi OS install. We will turn off the desktop window services later. 

A display will help you interact directly and monitor the device. A keyboard will let you use the command-line interface directly. A mouse enables you to use the full GUI. If you do not have or will never have a keyboard/mouse/display available you can go full headless. 
- <https://www.raspberrypi.org/documentation/configuration/wireless/headless.md>
- <https://www.raspberrypi.org/documentation/remote-access/ssh/README.md>

Once you have the SD Card imaged, insert it and plug in the keyboard, mouse, monitor (if available) and power. It may take 2 minutes for the initial boot.

If you are headless you will need to SSH into the Raspberry Pi and run 'sudo raspi-config' otherwise you can use the GUI version. The GUI initial boot will take you through the localization wizard.

The initial username/password will be pi/raspberry. The compuername will be raspberrypi.local.

From raspi-config you should make appropriate changes in the following areas.

- Network
- Change Password
- Computer Name
- Autologin
- SSH
- Localization
- TimeZone
- Keyboard
- I2C (for Motor Controller and Sensor Array)
- SPI (for Dislay)
- Camera (for RaspiCam)
- Update

SSH will now be 'ssh pi@<ComputerName>.local'

BTW,
I use 'tabs' as the white-space in my Python scripts because it isn't the 1990's anymore. Edit '~/.nanorc' and add the line 'set tabsize 4'.

This is a good time to get the Camera hooked up. Follow the directions specified for your camera. If it is a Raspi Cam and you wish the LED to be off, follow the instructions at <https://www.raspberrypi-spy.co.uk/2013/05/how-to-disable-the-red-led-on-the-pi-camera-module/>

Shutdown from GUI or execute 'sudo shutdown now'. You may unplug keyboard, etc and reboot by replugging in the power. Or you may continue using the desktop or turn it off in Raspi-config.

I have found these useful to install
sudo apt-get install git
sudo apt-get install p7zip-full
sudo apt-get install python-smbus python3-smbus python-dev python3-dev i2c-tools

The Train uses Low Energy Bluetooth (BLE) to communicate to external devices. There is a strage bug connection to iOS devices. 

'sudo nano /lib/systemd/system/bluetooth.service'
Add the bolded text yo the end of that line in the file.
ExecStart=/usr/lib/bluetooth/bluetoothd **-P battery**

I am using a framework called bluezero to make BLE easier to code. <https://pypi.org/project/bluezero>
sudo pip3 install bluezero

For sensing processor stats:
sudo pip3 install psutil

For saving preferences 
pip3 install appdirs

The hardware modules I am using, beyond the camera are:
<https://www.waveshare.com/wiki/Motor_Driver_HAT>
<https://www.waveshare.com/wiki/Sense_HAT_(B)>
<https://www.waveshare.com/wiki/2inch_LCD_Module>


git clone thejoveexpress

https://www.thegeekstuff.com/2008/11/3-steps-to-perform-ssh-login-without-password-using-ssh-keygen-ssh-copy-id/

https://apps.apple.com/us/app/rsyncinator/id1569680330?mt=12

rsync -rvh --progress --delete /Users/dave/thejoveexpress/pi/ pi@thejoveexpress.local:/home/pi/thejoveexpress/pi

https://www.raspberrypi.org/forums/viewtopic.php?t=190584
https://www.guidgenerator.com/online-guid-generator.aspx
https://www.bluetooth.com/specifications/assigned-numbers/

https://specificationrefs.bluetooth.com/assigned-values/Appearance%20Values.pdf


add "python3 thejoveexpress/pi/train.py &" to .profile



This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact
