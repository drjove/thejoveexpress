from bluezero import peripheral
from hardware.PCA9685 import PCA9685Driver
from btProperty import BTProperty
from fog.FogMessage import FogMessage
from fog.FogRational import FogRational

class Engine:
	def __init__(self, peripheral, serviceIdx, hardware, port, reversed):
		self.hardware = hardware
		self.port = port
		self.requestedPower = 0
		self.reversed = reversed

		self.calibrationProp = BTProperty(
			'01010000-0000-1000-8000-0080544A4578', 15,
			lambda message, value: FogRational.create(value, 100).encode(message),
			lambda message: FogRational.decode(message, True).normalized(100),
			saves=True)
		
		self.powerProp = BTProperty(
			'01010100-0000-1000-8000-0080544A4578', self.requestedPower,
			lambda message, value: FogRational.create(value, 100).encode(message),
			lambda message: FogRational.decode(message, True).normalized(100),
			saves=False)

		self.hardware.run(self.port, self.requestedPower)
		
		self.calibrationProp.activate(peripheral, serviceIdx, self.calibrate)
		self.powerProp.activate(peripheral, serviceIdx, self.run)

	def kill(self):
		self._run(0)

	def calibrate(self, calibration):
		calibration = int(calibration)
		if calibration < 0:
			calibration = 0
		elif calibration > 100:
			calibration = 100
		old = self.calibrationProp.value
		self.calibrationProp.value = calibration
		requestedPower = abs(self.requestedPower)
		if requestedPower <= calibration and requestedPower > old:
			self._run(0)
		if requestedPower > calibration and requestedPower <= old:
			self._run(self.requestedPower)

	def stop(self):
		self.run(0)
			
	def run(self, power):
		power = int(power)
		self.requestedPower = power
		self._run(power)
		
	def _run(self, power):
		if power < 0:
			if power < -100:
				power = -100
			elif power >= (self.calibrationProp.value * -1):
				power = 0
		elif power > 0:
			if power > 100:
				power = 100
			elif power <= self.calibrationProp.value:
				power = 0

		self.powerProp.value = power
		actualPower = power
		if reversed:
			actualPower = power * -1
		self.hardware.run(self.port, actualPower)
