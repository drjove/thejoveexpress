from hardware.CameraServer import CameraServer
from bluezero import peripheral
from btProperty import BTProperty
from fog.FogMessage import FogMessage

import socket
s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
s.connect(("8.8.8.8", 80))
ip = s.getsockname()[0]

class Camera:
	def __init__(self, peripheral, serviceIdx, port=8081):
		self.fpsProp = BTProperty(
			'01050000-0000-1000-8000-0080544A4578', 6,
			FogMessage.encodeUByte,
			FogMessage.decodeUByte,
			saves=True)
		self.powerProp = BTProperty(
			'01050100-0000-1000-8000-0080544A4578', False,
			FogMessage.encodeBool,
			FogMessage.decodeBool,
			saves=True)
		self.urlProp = BTProperty(
			'01050200-0000-1000-8000-0080544A4578', (ip, port),
			FogMessage.encodeIP,
			None,
			saves=False)
			
		self.server = CameraServer(port=port, frameRate=self.fpsProp.value)
		
		self.fpsProp.activate(peripheral, serviceIdx, self.fps)
		self.powerProp.activate(peripheral, serviceIdx, self.powered)
		self.urlProp.activate(peripheral, serviceIdx, None)
		
	def run(self):
		self.server.run(self.powerProp.value)
		
	def fps(self, calibrate):
		self.server.frameRate = calibrate
		self.fpsProp.value = calibrate
		
	def powered(self, power):
		self.server.paused = not power
		self.powerProp.value = power
		
	def kill(self):
		self.server.kill()
