//
//  BTScanner.swift
//  The Jove Express
//
//  Created by David Giovannini on 6/30/21.
//

import Foundation
import Combine
import CoreBluetooth

public protocol BTScannerDelegate: AnyObject {
	func peripheralDiscovered(_ peripheral: CBPeripheral, advertisementData: [String : Any], rssi RSSI: NSNumber)
	func peripheralConnected(_ peripheral: CBPeripheral)
	func peripheralConnectFailed(_ peripheral: CBPeripheral, _ error: Error?)
	func peripheralDisconnected(_ peripheral: CBPeripheral, _ error: Error?)
}

extension CBManagerAuthorization {
	var canStartScan: Bool {
		switch self {
		case .notDetermined:
			return true
		case .restricted:
			return false
		case .denied:
			return false
		case .allowedAlways:
			return true
		@unknown default:
			return false
		}
	}
}

public class BTScanner: NSObject, ObservableObject {
	private let centralManager : CBCentralManager
	private var wantScan: [BTServiceIdentity]?
	
	public weak var delegate: BTScannerDelegate?
	
	@Published public private(set) var scanning: Bool = false
	
	public override init() {
		centralManager = CBCentralManager(delegate: nil, queue: nil)
		super.init()
		centralManager.delegate = self
	}
	
	deinit {
		centralManager.stopScan()
	}
	
	public var authorization: CBManagerAuthorization {
		CBCentralManager.authorization
	}
	
	public func startScan(services: [BTServiceIdentity]) {
		self.wantScan = services
		if authorization.canStartScan && centralManager.state == .poweredOn {
			if !self.scanning {
				self.scanning = true
			}
			print("BTScanner start")
			centralManager.scanForPeripherals(withServices: services.map(\.identifer))
		}
	}
	
	public func stopScan() {
		self.wantScan = nil
		if self.scanning {
			self.scanning = false
			print("BTScanner stop")
			centralManager.stopScan()
		}
	}
	
	public func connect(device: CBPeripheral) {
		centralManager.connect(device, options: nil)
	}
	
	public func disconnect(device: CBPeripheral) {
		centralManager.cancelPeripheralConnection(device)
	}
}

extension BTScanner: CBCentralManagerDelegate {
	public func centralManagerDidUpdateState(_ central: CBCentralManager) {
		//print("BTScanner state: \(central.state)")
		switch central.state {
		case .unknown:
			break
		case .resetting:
			self.scanning = false
		case .unsupported:
			self.scanning = false
		case .unauthorized:
			self.scanning = false
		case .poweredOff:
			self.scanning = false
		case .poweredOn:
			if let wantScan = self.wantScan, !self.scanning {
				self.startScan(services: wantScan)
			}
			break
		@unknown default:
			break
		}
	}
	
	public func centralManager(_ central: CBCentralManager, didDiscover peripheral: CBPeripheral,advertisementData: [String : Any], rssi RSSI: NSNumber) {
		print("BTScanner: discovered \(peripheral.identifier) \(peripheral.name ?? "-")")
		self.delegate?.peripheralDiscovered(peripheral, advertisementData: advertisementData, rssi: RSSI)
	}
	
	public func centralManager(_ central: CBCentralManager, didConnect peripheral: CBPeripheral) {
		//print("BTScanner: connected \(peripheral.identifier)")
		delegate?.peripheralConnected(peripheral)
	}
	
	public func centralManager(_ central: CBCentralManager, didFailToConnect peripheral: CBPeripheral, error: Error?) {
		//print("BTScanner: failed connected \(peripheral.identifier)")
		delegate?.peripheralConnectFailed(peripheral, error)
	}
	
	public func centralManager(_ central: CBCentralManager, didDisconnectPeripheral peripheral: CBPeripheral, error: Error?) {
		//print("BTScanner: disconnected \(peripheral.identifier)")
		delegate?.peripheralDisconnected(peripheral, error)
	}
}
